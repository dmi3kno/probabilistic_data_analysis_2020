# Probabilistic data analysis: research seminar 2020

**Research seminar on the theory and practice of probabilistic data analysis**. We will go through selected parts of [Bayesian Data Analysis](http://www.stat.columbia.edu/~gelman/book/) (Gelman et al. 3rd edition), complementary material and probabilistic programming assignments with R and RStan.

**Participation** Open to committed participants that have suitable (PhD study level or equivalent) background in a relevant field (maths / stats / computational and data sciences). Arranged by [Turku Data Science group](http://www.iki.fi/Leo.Lahti). 

**Contact** Leo Lahti <first.last@utu.fi> (Associate Prof. / Data Science) in order to join, or to discuss practicalities (study credits?). 

**Time** Friday at 12:15. Every second (even) week. Starting March 20, 2020.

**Location** Virtual ([Zoom](zoom.md)) meeting. The opencomp Slack channel #readingcircle can be used for online chatting. Invitations will be sent to the accepted participants.

**Schedule** [Schedule and links to the material](schedule.md) 







