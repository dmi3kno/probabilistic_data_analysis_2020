# Research seminar on probabilistic data analysis: Session 5 (May 15, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)



# Discussion topic 1 (Dmytro Perepolkin): Stan


# Discussion topic 2 (Iiro Tiihonen): Demo
    * The demo is about applying Stan to fitting Gaussian Process models
    * For the purposes of this demo, a good introduction to
    Gaussian Process modeling is given by (https://royalsocietypublishing.org/doi/full/10.1098/rsta.2011.0550)
    * For fitting such models with Stan, please read the related chapter 10 of the official Stan guide (https://mc-stan.org/docs/2_23/stan-users-guide/gaussian-processes-chapter.html)
    * I might come up with additional demonstrative material later, but understanding the core ideas presented in the introduction text
    and the Stan implementation of the guide is enough.