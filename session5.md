# Research seminar on probabilistic data analysis: Session 5 (May 15, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)


# General Reading
*  Aki's lectures 8 and 9 [Lectures](https://github.com/avehtari/BDA_course_Aalto/tree/master/slides)
*  "Visualization in Bayesian Workflow" [Gabry et al. (2017)](https://arxiv.org/abs/1709.01449)

# Discussion topic 1 (Raoul Wolf): Model checking
*  (Visual) Posterior Predictive Checks 
    * Read Chapters 6.3 and 6.4 (and re-read Paul's lecture from last week if needed)
    * Skim through Aki's lecture slides [slides](https://github.com/avehtari/BDA_course_Aalto/blob/master/slides/slides_ch6.pdf)
    * Browse the "bayesplot" vignette [vignette](https://cran.r-project.org/web/packages/bayesplot/vignettes/graphical-ppcs.html)
* Model sensitivity and accuaracy
    * Read chapters 7.1 and Vehtari, Gelman and Gabry (2017) [link](http://www.stat.columbia.edu/~gelman/research/unpublished/loo_stan.pdf) (Aki recommends reading this over 7.2, as the paper is more up-to-date)
    * Skim through [rat-demo](https://avehtari.github.io/modelselection/rats_kcv.html )
    * Recommended general overview reading for model checking and [CV-FAQ](https://avehtari.github.io/modelselection/CV-FAQ.html)
* Bayes R^2
    * Skim through R^2 [demo](https://avehtari.github.io/bayes_R2/bayes_R2.html)
    * If you want more background, read the [pre-print](http://www.stat.columbia.edu/~gelman/research/unpublished/bayes_R2_v3.pdf) 

# Discussion topic 2 (David Kohns): Feature selection
*  Variable selection through Bayes Factor
    * Read chapter 7.4
* Rationale for model expansion
    * Read chapter 7.5 (discussion will be kept very brief)
* Variable selection through sparsity priors
    * Read through [demo](https://betanalpha.github.io/assets/case_studies/bayes_sparse_regression.html)
    * Background information (not mandatory, but encouraged if interested)
        * Good overview of competing sparsity priors: [Piironen & Vehtari (2017)](https://projecteuclid.org/euclid.ejs/1513306866)
        * Theoretical foundation for modern variable selection techniques: [Scott & Polson (2010)](https://faculty.chicagobooth.edu/nicholas.polson/research/papers/Bayes1.pdf)
* Projective inference in high dimensional problems
    * Read through [slides](https://github.com/avehtari/BDA_course_Aalto/blob/master/slides/slides_ch7b.pdf)
    * Skim through [demo](https://avehtari.github.io/modelselection/bodyfat.html)
    * Background information (not mandatory) [Piironen, Paasiniemi and Vehtari (2018)](https://arxiv.org/abs/1810.02406) and [Projpred vs. marginal posterior probabilities](https://link.springer.com/article/10.1007%2Fs11222-016-9649-y)
* Model averaging and stacking: if time permits
    * Read through [Yao, Vehtari, Simpson and Gelman (2018)](https://projecteuclid.org/euclid.ba/1516093227)
    * For those interested a general overview over selection methods and model averaging [Vehtari and Ojanen (2012)](https://projecteuclid.org/download/pdfview_1/euclid.ssu/1356628931)
    * For a demo see [here](http://mc-stan.org/loo/articles/loo2-weights.html)
